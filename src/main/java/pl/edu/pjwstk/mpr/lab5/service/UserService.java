package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.List;
import java.util.Map;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {

    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
        
    }

    public static Person findOldestPerson(List<User> users) {
        
    }

    public static User findUserWithLongestUsername(List<User> users) {
        
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
        
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
        
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
        
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
        
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        
    }
}
